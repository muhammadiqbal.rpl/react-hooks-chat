import React from "react";

import "./Layout.css";

const Layout = (props) => {
  const { children } = props;

  return (
    <div className="layout">
      <div className="content-layout">
        {children}
      </div>
    </div>
  );
}
export default Layout;