import React from "react";

import "./Field.css";

const Field = (props) => {
  const { type, value, className, placeholder, onChange } = props;

  return (
    <>
      <input
        type={type ? type : 'text'}
        className={`input-box ${className}`}
        value={value}
        onChange={onChange}
        placeholder={placeholder ? placeholder : 'Type here...'}
      />
    </>
  );
}
export default Field;