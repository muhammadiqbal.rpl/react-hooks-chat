import React from "react";

import "./Button.css";

const Button = (props) => {
  const { children, className, onClick } = props;

  return (
    <>
      <div
        className={`btn-box ${className}`}
        onClick={onClick}
      >
        {children}
      </div>
    </>
  );
}
export default Button;