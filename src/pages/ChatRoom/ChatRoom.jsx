import React, { useState } from "react";

import useChat from "../../helper/useChat";

import Button from "../../components/Button/Button";
import "./ChatRoom.css";

const ChatRoom = (props) => {
  const { roomName } = props.match.params;
  const { messages, sendMessage } = useChat(roomName);
  const [newMessage, setNewMessage] = useState("");

  const handleSendMesagge = () => {
    sendMessage(newMessage);
    setNewMessage("");
  }

  return (
    <div className="chat-room-page">
      <div className="room-name">Room : {roomName}</div>

      <div className="message-container">
        <div className="messages-list">
          {
            messages.map((message, key) => (
              <div
                key={key}
                className={`message-item ${message.ownedByCurrentUser ? "my-message" : "received-message"}`}
              >
                {message.body}
              </div>
            ))
          }
        </div>
      </div>
      <div className="send-message">
        <textarea rows="1" className="input-send" value={newMessage} onChange={(e) => setNewMessage(e.target.value)} placeholder="Type message..."></textarea>
        <Button onClick={handleSendMesagge} className="cursor-pointer btn-send">Send</Button>
      </div>
    </div>
  );
}
export default ChatRoom;