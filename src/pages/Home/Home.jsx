import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';

import Field from '../../components/Field/Field';
import Button from '../../components/Button/Button';
import "./home.css";

const Home = (props) => {
  const [roomName, setRoomName] = useState('')

  return (
    <div className="home-page">
      <Field
        className="input-custom"
        placeholder="Room"
        type="text"
        onChange={(e) => setRoomName(e.target.value)}
      />
      <Button onClick={() => props.history.push(`/${roomName}`)} className="cursor-pointer">Join Room</Button>
    </div>
  );
}
export default withRouter(Home);