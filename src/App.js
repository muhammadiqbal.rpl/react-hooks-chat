import React from "react";
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Layout from "./components/Layout/Layout"
import Home from "./pages/Home/Home"
import chatRoom from "./pages/ChatRoom/ChatRoom"

const App = () => {
  return (
    <Layout>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/:roomName" exact component={chatRoom} />
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;
