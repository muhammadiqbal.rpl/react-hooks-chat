const server = require("http").createServer();

const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  }
});

const PORT = 4000;
const NEW_CHAT_MESSAGE_EVENT = "newChatMessage";

io.on("connection", (socket) => {
  // join conversation
  const { roomName } = socket.handshake.query;
  socket.join(roomName);

  // listen new message
  socket.on(NEW_CHAT_MESSAGE_EVENT, (data) => {
    io.in(roomName).emit(NEW_CHAT_MESSAGE_EVENT, data);
  });

  // leave room
  socket.on("disconnect", () => {
    socket.leave(roomName);
  });
});

server.listen(PORT, () => {
  console.log(`Listening on PORT: ${PORT}`);
});